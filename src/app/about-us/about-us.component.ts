import { Component, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor(private meta: Meta) {
    this.meta.updateTag({name: "title", content: "About US Titulo"});
    this.meta.updateTag({name: "description", content: "Descripcion"});
    this.meta.updateTag({name: "theme-color", content: "#000000"});
  }

  ngOnInit() {
  }

}
