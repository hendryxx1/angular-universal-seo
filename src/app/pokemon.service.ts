import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscriber, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  API_PREFIX = "https://pokeapi.co/api/v2/";

  constructor(private http: HttpClient) { }

  getPokemones(): Observable<any>{
    return this.http.get(`${this.API_PREFIX}pokemon`)
      .pipe(map(x => x['results']));
  }

}
