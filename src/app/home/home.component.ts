import { Component, OnInit } from '@angular/core';
import { SeoService } from '../seo.service';
import { Meta } from '@angular/platform-browser';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pokemones: string[];

  constructor(private meta: Meta, private pokemonService: PokemonService) {
    this.meta.updateTag({name: "title", content: "HOME Titulo"});
    this.meta.updateTag({name: "description", content: "Descripcion"});
    this.meta.updateTag({name: "theme-color", content: "#000000"});
  }

  ngOnInit() {
    this.pokemonService.getPokemones().subscribe(
      resp => {
        console.log(resp);
        this.pokemones = resp;
      },
      err =>{ 
        console.log(err)
      }
    )


  }

}
