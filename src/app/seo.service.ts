import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';


interface Config {
  title?: string;
  description?: string;
  themeColor?: string;
}

@Injectable({
  providedIn: 'root'
})
export class SeoService {

  constructor(private meta: Meta){}

  public seoGenerate(config: Config){
    this.meta.addTag({name: "title", content: config.title});
    this.meta.addTag({name: "description", content: config.description});
    this.meta.addTag({name: "theme-color", content: config.themeColor});
  }

}
