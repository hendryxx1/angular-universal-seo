import { Component, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-gracias',
  templateUrl: './gracias.component.html',
  styleUrls: ['./gracias.component.css']
})
export class GraciasComponent implements OnInit {

  constructor(private meta: Meta) { 
    this.meta.updateTag({name: "title", content: "Gracias Titulo"});
    this.meta.updateTag({name: "description", content: "Descripcion"});
    this.meta.updateTag({name: "theme-color", content: "#000000"});
  }

  click(){
    
  }

  ngOnInit() {
    
  }

}
