import { Component, OnInit } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private meta: Meta) {
    this.meta.updateTag({name: "title", content: "Contacto Titulo"});
    this.meta.updateTag({name: "description", content: "Descripcion"});
    this.meta.updateTag({name: "theme-color", content: "#000000"});
  }

  ngOnInit() {
  }

}
